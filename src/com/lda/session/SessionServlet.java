package com.lda.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionServlet
 */
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public SessionServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		String id = session.getId();
		System.out.println("id:"+id);
		
		Map sessionsMap=null;
		sessionsMap=(Map) request.getServletContext().getAttribute("sessions");
		if(sessionsMap==null){
			sessionsMap=new HashMap();
		}else{
			
			for (Iterator iterator = sessionsMap.keySet().iterator(); iterator.hasNext();) {
				String id_i = (String) iterator.next();
				System.out.println("Intentando invalidar:"+ id_i);
				HttpSession session_i=(HttpSession) sessionsMap.get(id_i);
				try{
					if(!id.equals(id_i)){
						session_i.invalidate();
						System.out.println("Session invalidada");
					}else{
						System.out.println("sesion actual, no invalidamos");
					}
					
				}catch(java.lang.IllegalStateException i){
					System.out.println("Session ya invalidada previamente:"+id_i);
				}
			}
		}
		sessionsMap.put(id, session);
		request.getServletContext().setAttribute("sessions", sessionsMap);
		
	}

}
